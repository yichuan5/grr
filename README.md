GRR calculation
Description:
  This program calculated repeatability and reproducibility from inputed STDF files.  

Launch:
	Rscript grr.r -f [stdf_file1],[stdf_file2]

requirement:
	2 or more stdf files contain same number of dies
	
	R version 3.2.1 or greater

packages:
(if any packages are missing, program will attempt to install in default lib, from CRAN)
	data.table
	SixSigma
	optparse

Options:
	-f CHARACTER, --filelist=CHARACTER
		at least 2 (comma separated) STDF files

	-s CHARACTER, --select=CHARACTER
		regular expression of filterd tests

	-r CHARACTER, --remove=CHARACTER
		regular expression of removed tests

	-n CHARACTER, --name=CHARACTER
		(comma separated) Names for STDF files in log [default= 1,2,..]

	-o CHARACTER, --out=CHARACTER
		output PDF and TXT file name [default: GRR]

	-h, --help
		Show this help message and exit

	
