if(!"data.table"%in%rownames(installed.packages())) ##maybe combine into 1
{install.packages("data.table",repos = "http://cran.us.r-project.org",
                  dependencies=TRUE)}

if(!"SixSigma"%in%rownames(installed.packages()))
{install.packages("SixSigma",repos = "http://cran.us.r-project.org",
                  type='source', dependencies=TRUE)}

if(!"optparse"%in%rownames(installed.packages()))
{install.packages("optparse",repos = "http://cran.us.r-project.org",
                  type='source', dependencies=TRUE)}

library(data.table)
library(SixSigma)
library(optparse)
options(max.print=10000)
#this function check all arguments, 
#return with value opt, and output errors and helps
getArgs<-function()
{
  option_list = list(
  make_option(c("-f", "--filelist"), type="character", default=NULL, 
              help="at least 2 comma separated STDF files", metavar="character"),
  make_option(c("-s", "--select"), type="character", default=NULL,
              help="regular expression of filterd tests", metavar="character"),
  make_option(c("-r", "--remove"), type="character", default=NULL,
              help="regular expression of removed tests", metavar="character"),
  make_option(c("-n", "--name"), type="character", default=NULL,
              help="(comma separated) Names for STDF files in log [default= 1,2,..]", metavar="character"),
  make_option(c("-o", "--out"), type="character", default="GRR", 
              help="output PDF and TXT file name [default: %default]", metavar="character")) 
    
  opt_parser = OptionParser(option_list=option_list,description=
      "This Program calculate GRR and other value for repeatability and reproudction.")
  args<-commandArgs(trailingOnly = TRUE)
  tryCatch({opt = parse_args(opt_parser)},
             error =   function(e){
             cat(paste("\nThe argument you pass",args,"is incorrect.\n"))
             cat( "Lauch help file at \"Rscript grr.r -h\"\n\n")
             quit()})
  return(opt)
}
#this function check does the arguments make sense,
#and parse filelist and filenames
checkArgs<-function(opt)
{
  if (is.null(opt$filelist))
  {
    cat("\nNo input file, help file at \"Rscript grr.r -h\"\n\n")
    quit()
  }else
  { 
    myfilelist <-unlist(strsplit(opt$file, ","))
      if(length(myfilelist)<2){
        cat("\nNeed two STDF file seperated by only comma (input file).\n\n")
        quit()}
  }
  if (!is.null(opt$name)){
    myfilename<-unlist(strsplit(opt$name, ","))
    nname<-length(myfilename)
    nfile<-length(myfilelist)
    if (nname!=nfile)
    {
      cat(paste("There are ",nname," file names, and ",nfile," files."))
      quit()
    }
  }
  parsedArg<-list(myfilelist=myfilelist,myfilename=myfilename)
  return(parsedArg)
}


#this function the location of this .R script 
LocationOfThisScript = function() 
{
  this.file = NULL
  # If this file may be 'sourced'
#   for (i in -(1:sys.nframe())) {
#     if (identical(sys.function(i), base::source)) this.file = (normalizePath(sys.frame(i)$ofile))
#   }
#   if (!is.null(this.file)) return(dirname(this.file))
  
  # But it may also be called from the command line
  cmd.args = commandArgs(trailingOnly = FALSE)
  cmd.args.trailing = commandArgs(trailingOnly = TRUE)
  cmd.args = cmd.args[seq.int(from=1, length.out=length(cmd.args) - length(cmd.args.trailing))]
  res = gsub("^(?:--file=(.*)|.*)$", "\\1", cmd.args)

  # If multiple --file arguments are given, R uses the last one
  res = tail(res[res != ""], 1)
  if (0 < length(res)) return(dirname(res))
  
  # Both are not the case. Maybe we are in an R GUI?
  return(NULL)
}
current.dir = LocationOfThisScript()
source(paste(current.dir,"ConvertStdf.R",sep="/"))

MakeStdfTable<-function(mylist)
{#from converted list, output a result table with testnames as colname
  mytable<-mylist$ResultsMatrix        
  colnames(mytable)<-charSub(mylist$ParametersFrame$testname)
  mytable<-data.table(mytable)
  return(mytable)
}
# mytable<-rbind(mytable,mytable,mytable)  #dulicate to mimic repeat testing 
splitTable<-function(mytable)
{#add runNum and dieNum from XY position repeat, If a sample file exist we 
 #can obtain from DeviceFrame directly inside of calculating.
  x<-mytable$X[1]
  y<-mytable$Y[1]                              #first die position
  head<-which(mytable$X==x&mytable$Y==y[1])    #find all same position
  tableLength<-nrow(mytable)/length(head)      #size of one test run
  mytable$runNum<-rep(seq_along(head),each=tableLength)
  mytable$dieNum<-rep(1:tableLength,time=length(time))
  return(mytable)
}
#filter out die over six sigma range  CURENTLY NOT USED
filter<-function (mytable,testnames)
{
  trueTable<-sapply(seq_along(testnames),
                    function(i){filterSigma(mytable[,get(testnames[i])])})
                                                    #filtered boolean table
  index<-apply(trueTable,1,sum)==length(testnames)  #combine result for all test
              #get pass chip 

  maxDieNum<-max(mytable$dieNum)                   #num of die in a table
  maxTable<-nrow(mytable)/maxDieNum                #total num of table
                #This is prone to error

  dim(index)<-c(maxDieNum,maxTable)                #reshape
  index2<-which(apply(index,1,sum)==maxTable)   #combine result for all table, get index
  return(mytable[dieNum%in%index2,])            #get die num from index, changeme: dumb 
}
#custom filter detect outlier base on sigma CURRENTLY NOT USED
filterSigma<-function (x)
{
  sig<-.9938 #current 3sigma:0.933, 4sigma:0.9938
  low<-quantile(x, (1-sig)/2)
  high<-quantile(x, (1-(1-sig)/2))
  return(x>low&x<high)
}
selectTest<-function(mytable,pattern,remove)
{
  testnames<-colnames(mytable)
  if (is.null(remove)){nselect<-rep(TRUE,times=length(testnames))} #nothing to remove
  else {nselect<-!grepl(remove,testnames)} #removed
  
  if (is.null(pattern)){select<-rep(TRUE,times=length(testnames))}
  else {select<-grepl(pattern,testnames)}   #selected
  combine<-nselect & select
  smalltable<-mytable[,..combine]
  smalltable<-removeIdentical(smalltable)
  return(smalltable)
}
#remove the tests whose result doesn't change, avoid error in ss.rr
removeIdentical<-function(mytable)
{
  index<-sapply(1:ncol(mytable),function(i)
      {nrow(unique(mytable[,..i]))>1})
  mytable<-mytable[,index,with=FALSE]
  return(mytable)
}
#substitute - with _, avoid error in ss.rr 
charSub<-function(stringvector)
{
  gsub("-","_",stringvector)
}
#this funtion take the ParametersFrame of STDF list 
#output ll,ul table, not added because not filtered
getlimit<-function(ParametersFrame)
{
  testrows<-c(which(ParametersFrame$testnum==1):nrow(ParametersFrame))
  return(ParametersFrame[testrows,c("ll","ul")])
}
################main############################
opt<-getArgs()
parsedArg<-checkArgs(opt)
mytable<-NULL

for (i in seq_along(parsedArg$myfilelist))
{
  cat(paste("\nStarting STDF files conversion."))
  mylist<-ConvertStdf(parsedArg$myfilelist[i]) 
  temptable<-MakeStdfTable(mylist)
  temptable<-selectTest(temptable,opt$select,opt$remove)
  if(is.null(temptable$X)|(sum(temptable$X)==0))# if no position or empty position info
  {
    temptable$dieNum<-(1:nrow(temptable))     #1:end die
    temptable$runNum<-1                       #No repeat
    x<-temptable                            #(delete) mimic repeat
    x[,1:((ncol(x)-2))]<-1.0001*x[,1:((ncol(x)-2))] #(delete) mimic repeat
    temptable<-rbind(temptable,x)     #(delete) duplicate to mimic repeat
  }else
  {
    temptable<-splitTable(temptable)
  }
  
  if (is.null(parsedArg$myfilename)){temptable$testSetup<-i}
  else{temptable$testSetup<-parsedArg$myfilename[i]}   #set Setup_name for W/ or W/O filename
  
  if(i==1){                                 #when parse first file
    mytable<-temptable
  }else{
    remain<-intersect(colnames(temptable),colnames(mytable))
    mytable<-rbind(mytable[,..remain],temptable[,..remain])
  }
  rm(temptable)
}

# limits<-getlimit(mylist$ParametersFrame)

#####################Calculate and Display###################################
smalltable<-mytable
smalltable$dieNum<-as.factor(smalltable$dieNum)
smalltable$testSetup<-as.factor(smalltable$testSetup)

pdfout<-pdf(paste(opt$out,".pdf",sep=""))                       #PDF graphic log
sink(paste(opt$out,".txt",sep=""),split=TRUE)                   #txt log
tableColNames<-colnames(smalltable)
resultMatrix<-sapply(1:(ncol(smalltable)-4), 
#-4 is to remove last added index: TestSort,dieNum,runNum,testSetup  
    function(i){    
            cat(paste("\n\n\n",tableColNames[i],"\n\n"))
            grrResult<-ss.rr(tableColNames[i],dieNum,
                          testSetup,data=smalltable)
            return(c(grrResult$studyVar[1,3],grrResult$varComp[1,2]))
            })

resultMatrix<-data.table(t(resultMatrix))
colnames(resultMatrix)<-c("study_Var_Percent","Total_GRR_Percent_Contrib")
resultMatrix$Test_Names<-colnames(smalltable)[1:(ncol(mytable)-4)] #remove last index and sortbin

cat("\n\nTotal GRR Percent Contribution Results\n\n")
breaks <- c(100,30,10,0)
mycut<-cut(resultMatrix$Total_GRR_Percent_Contrib,breaks=breaks)
outputlist<-split(resultMatrix,mycut,drop=TRUE)
print(outputlist,nrow=1000)

sink()
dev.off()

cat(paste("Program completed.\n")) 
cat(paste("Plots are saved in ",opt$out,".pdf\n",sep=""))
cat(paste("Result and logs are saved in ",opt$out,".txt\n\n",sep=""))
